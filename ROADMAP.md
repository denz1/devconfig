VERSIONS
=======
0.0.4
-----
* `YamlFileLoader.__del__` bugs
* constructors tests and refactoring
* constructors plugins with `entry_points`
* python-credstash constructors
* yaml modules cache description when yaml modules merged with `devconfig.merge` (merged yaml module members not cached and cant do so)
* mysql layer with sqlalchemy


0.3.2
-----
* fix: django on py27
* fix: module files load order
* YamlFileLoader now supports `.json`

0.0.3
-----
* config of config
* python-credstash layer (without key versioning)
* hooks `before_module_exec` and `after_module_exec`
* python2.7 tests

0.0.2
-----
* constructors
* importlib to import yamls

0.0.1
-----
* anchors sharing
* mappings merge

